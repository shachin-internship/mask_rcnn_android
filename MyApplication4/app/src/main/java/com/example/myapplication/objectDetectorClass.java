package com.example.myapplication;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;

import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.gpu.GpuDelegate;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class objectDetectorClass {
    //load the model
    public Interpreter interpreter;
    //store all label
    private List<String> labelList;
    private GpuDelegate gpuDelegate;




    objectDetectorClass(AssetManager assetManager , String modelPath , String labelPath) throws IOException{
        //load model
        interpreter = new Interpreter(loadModelFile(assetManager , modelPath));
        //load labelmap
        labelList = loadLabelList(assetManager , labelPath);
    }

    //get molded_images
    public float[][][][] image_mold(Bitmap bitmap){

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int componentsperPixel = 3;
        int totalPixels = width * height;

        float[] input = new float[totalPixels * componentsperPixel];
        float[][][] inputs = new float[height][width][componentsperPixel];
        int[] rgbpixel = new int[totalPixels];
        bitmap.getPixels(rgbpixel, 0, width, 0, 0, width,height);
        for(int i = 0 ; i < totalPixels ; i++){
            int pixel = rgbpixel[i];
            int red = Color.red(pixel);
            int green = Color.green(pixel);
            int blue = Color.blue(pixel);
            input[i * componentsperPixel + 0] = red;
            input[i * componentsperPixel + 1] = green;
            input[i * componentsperPixel + 2] = blue;
        }
        int l = 0;
        for(int i = 0 ; i < height ; i++){
            for(int j = 0 ; j < width ; j++){
                for(int k = 0 ; k < componentsperPixel ; k++){
                    inputs[i][j][k] = input[l++];
                }
            }
        }

        float[][][][] molded_image = molded_image(inputs , bitmap);

        return molded_image;
    }

    //get image_meta
    public float[][] image_metas(Bitmap bitmap){

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int componentsperPixel = 3;
        int totalPixels = width * height;

        float[] input = new float[totalPixels * componentsperPixel];
        float[][][] inputs = new float[height][width][componentsperPixel];
        int[] rgbpixel = new int[totalPixels];
        bitmap.getPixels(rgbpixel, 0, width, 0, 0, width,height);
        for(int i = 0 ; i < totalPixels ; i++){
            int pixel = rgbpixel[i];
            int red = Color.red(pixel);
            int green = Color.green(pixel);
            int blue = Color.blue(pixel);
            input[i * componentsperPixel + 0] = red;
            input[i * componentsperPixel + 1] = green;
            input[i * componentsperPixel + 2] = blue;
        }
        int l = 0;
        for(int i = 0 ; i < height ; i++){
            for(int j = 0 ; j < width ; j++){
                for(int k = 0 ; k < componentsperPixel ; k++){
                    inputs[i][j][k] = input[l++];
                }
            }
        }



        float[][] image_meta = image_meta(inputs , bitmap);
        return image_meta;
    }
    
    private List<String> loadLabelList(AssetManager assetManager , String labelPath) throws IOException{
        //to store label
        List<String> labelList = new ArrayList<>();
        //crete a new reader
        BufferedReader reader = new BufferedReader(new InputStreamReader(assetManager.open(labelPath)));
        String line;
        //loop through each line and store in labelList
        while((line = reader.readLine()) != null){
            labelList.add(line);
        }
        reader.close();
        return labelList;

    }

    private ByteBuffer loadModelFile(AssetManager assetManager , String modelPath) throws  IOException{
        //use to get description of file
        AssetFileDescriptor fileDescriptor = assetManager.openFd(modelPath);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startoffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();

        return fileChannel.map(FileChannel.MapMode.READ_ONLY , startoffset , declaredLength);
    }

     static float[] extractColorChannels(int pixel) {

        float b = ((pixel)       & 0xFF);
        float g = ((pixel >>  8) & 0xFF);
        float r = ((pixel >> 16) & 0xFF);

        return new float[] {r,g,b};
    }

    public float[][][][] molded_image(float [][][] images , Bitmap bitmap){
        float[][][] molded_image = new float[1024][1024][3];

        int min_dim = 800;
        int min_scale = 0;
        int max_dim = 1024;
        int k = 0;
        String mode = "square";
        molded_image = resize_image(images , min_dim , min_scale , max_dim , mode , bitmap);

        molded_image = mold_image(molded_image , null);
        float [][][][]molded_images = {molded_image};

        return molded_images;
    }

    //get the windows
    public float[] window(float[][][] image , Bitmap bitmap1){
        float[] window = new float[4];
        int min_dim = 800;
        int min_scale = 0;
        int max_dim = 1024;
        int k = 0;
        String mode = "square";
        window = get_window(image , min_dim , min_scale , max_dim ,mode , bitmap1);


        return window;
    }


    private float[][][] resize_image(float[][][] image, int min_dim, float min_scale, int max_dim, String mode , Bitmap bitmap1) {

        int h = image.length;
        int w = image[0].length;
        float h1 = image.length;
        float w1 = image[0].length;
        float mini_dim = (float) min_dim;
        float maxi_dim = (float) max_dim;
        float scale = 1.0f;
        float window[] = new float[4];

        if(min_dim != 0){
            scale = Math.max(1 , mini_dim / Math.min(h1 , w1));
        }
        if(min_scale != 0 && scale < min_scale){
            scale = min_scale;

        }
        if(max_dim != 0 && mode == "square"){
            int image_max = Math.max(h,w);
            if(Math.round(image_max * scale) > max_dim){
                scale = maxi_dim / image_max;
            }
        }

        if(scale != 1){
            float [] round = { Math.round(h * scale),  Math.round(w * scale)};

            image = resize(bitmap1 , (int)round[0] , (int)round[1]);

        }

        if(mode == "square"){
            h = image.length;
            w = image[0].length;
            int top_pad = (max_dim - h) / 2;

            int bottom_pad = max_dim - h - top_pad;
            int left_pad = (max_dim - w) / 2;

            int right_pad = max_dim - w - left_pad;
            int [][]padding = {{top_pad , bottom_pad} , {left_pad , right_pad} , {0,0}};
            int padwith = 0;
            image = pad(image , padwith , left_pad , right_pad , top_pad , bottom_pad);

            window = new float[]{top_pad, left_pad, h + top_pad, w + left_pad};

        }

        return image;
    }

    //resize the image to desired size
    public float[][][] resize(Bitmap bitmap , int input_height , int input_width){

        int[] intValues = new int[ (input_height * input_width)];

        Bitmap bitmap1;
        bitmap1 = Bitmap.createScaledBitmap(bitmap, input_width, input_height, false);

        int width = bitmap1.getWidth();
        int height = bitmap1.getHeight();

        int componentsperPixel = 3;
        int totalPixels = width * height;

        float[] input = new float[totalPixels * componentsperPixel];
        float[][][] inputs = new float[height][width][componentsperPixel];
        int[] rgbpixel = new int[totalPixels];
        bitmap1.getPixels(rgbpixel, 0, width, 0, 0, width,height);
        for(int i = 0 ; i < totalPixels ; i++){
            int pixel = rgbpixel[i];
            int red = Color.red(pixel);
            int green = Color.green(pixel);
            int blue = Color.blue(pixel);
            input[i * componentsperPixel + 0] = red;
            input[i * componentsperPixel + 1] = green;
            input[i * componentsperPixel + 2] = blue;
        }
        int l = 0;
        for(int i = 0 ; i < height ; i++){
            for(int j = 0 ; j < width ; j++){
                for(int k = 0 ; k < componentsperPixel ; k++){
                    inputs[i][j][k] = input[l++];
                }
            }
        }

        return inputs;
    }
    //pad the image
    private float[][][] pad(float[][][] image , int padwith , int left , int right , int top , int bottom){
        float [][][] temp = new float[image.length + top + bottom][image[0].length + left + right][3];

        for(int i = 0 ; i < temp.length ; i++)
        {
            for(int j = 0 ; j < temp[i].length ; j++){
                for(int k = 0 ; k < 3 ; k++){
                    temp[i][j][k] = padwith;
                }
            }
        }
        for(int i = 0 ; i < image.length ; i++){
            for(int j = 0 ; j < image[0].length ; j++){
                for(int k = 0 ; k < 3 ; k++){
                    temp[i + top][j + left][k] = image[i][j][k];
                }
            }
        }
        return temp;
    }

    public float[] get_window(float[][][] image, int min_dim, int min_scale, int max_dim, String mode , Bitmap bitmap1) {
        int h = image.length;
        int w = image[0].length;
        float h1 = image.length;
        float w1 = image[0].length;
        float mini_dim = (float) min_dim;
        float maxi_dim = (float) max_dim;
        float scale = 1.0f;
        float window[] = new float[4];

        if(min_dim != 0){
            scale = Math.max(1 , mini_dim / Math.min(h1 , w1));
        }
        if(min_scale != 0 && scale < min_scale){
            scale = min_scale;
        }
        if(max_dim != 0 && mode == "square"){
            int image_max = Math.max(h,w);
            if(Math.round(image_max * scale) > max_dim){
                scale = maxi_dim / image_max;
            }
        }

        if(scale != 1){
            float [] round = { Math.round(h * scale),  Math.round(w * scale)};

            image = resize(bitmap1 , (int)round[0] , (int)round[1]);

        }

        if(mode == "square"){
            h = image.length;
            w = image[0].length;
            int top_pad = (max_dim - h) / 2;
            int bottom_pad = max_dim - h - top_pad;
            int left_pad = (max_dim - w) / 2;
            int right_pad = max_dim - w - left_pad;
            int [][]padding = {{top_pad , bottom_pad} , {left_pad , right_pad} , {0,0}};
            int padwith = 0;
            image = pad(image , padwith , left_pad , right_pad , top_pad , bottom_pad);

            window = new float[]{top_pad, left_pad, h + top_pad, w + left_pad};

        }

        return window;

    }

    private float scale(float[][][] image, int min_dim, int min_scale, int max_dim, String mode , Bitmap bitmap1) {
        int h = image.length;
        int w = image[0].length;
        float scale = 1.0f;
        float mini_dim = (float) min_dim;
        float maxi_dim = (float) max_dim;
        float h1 = (float) h;
        float w1 = (float) w;

        int window[] = new int[4];
        if(min_dim != 0){
            scale = Math.max(1 , mini_dim / Math.min(h1 , w1));
        }
        if(min_scale != 0 && scale < min_scale){
            scale = min_scale;

        }
        if(max_dim != 0 && mode == "square"){
            int image_max = Math.max(h,w);
            if(Math.round(image_max * scale) > max_dim){
                scale = max_dim / image_max;

            }
        }

        if(scale != 1){
            boolean preserve_range = true;
            double [] round = { Math.round(h * scale),  Math.round(w * scale)};

            image = resize(bitmap1 , (int)round[0] , (int)round[1]);

        }

        if(mode == "square"){
            h = image.length;
            w = image[0].length;
            int top_pad = (max_dim - h) / 2;
            int bottom_pad = max_dim - h - top_pad;
            int left_pad = (max_dim - w) / 2;
            int right_pad = max_dim - w - left_pad;
            int [][]padding = {{top_pad , bottom_pad} , {left_pad , right_pad} , {0,0}};
            int padwith = 0;
            image = pad(image , padwith , left_pad , right_pad , top_pad , bottom_pad);

            window = new int[]{top_pad, left_pad, h + top_pad, w + left_pad};

        }

        return scale;
    }

    private float[] compose_image_meta(int image_id ,int[] original_image , int[] molded_image , float[] window , float scale , int[] zeros){
        float[] meta = new float[25];
        int k = 0;
        for(int a = 0 ; a < 25 ; a++){
            meta[a] = 0;
        }
        meta[k++] = image_id;
        for(int i : original_image)
        {
            meta[k++] = i;
        }
        for(int i: molded_image){
            meta[k++] = i;
        }
        for(float i: window){
            meta[k++] = i;
        }
        meta[k++] = scale;
        for(int i : zeros){
            meta[k++] = i;
        }



        return meta;
    }

    public float[][] image_meta(float [][][] images , Bitmap bitmap){
        float[] image_meta = new float[25];
        int zeros[] = {0,0,0,0,0,0,0,0,0,0,0,0,0};
        float[][][] molded_image = new float[1024][1024][3];
        float [][][][] molded_images = new float[1][1024][1024][3];
        int min_dim = 800;
        int min_scale = 0;
        int max_dim = 1024;
        int k = 0 , i = 0 , h , w , h1 , w1;

        String mode = "square";

        molded_image = resize_image(images , min_dim , min_scale , max_dim , mode , bitmap);

            molded_image = mold_image(molded_image , null);
            h = images.length;

            w = images[0].length;

            h1 = molded_image.length;
            w1 = molded_image[0].length;


        int []image_shape = {h , w , 3};
        int[] molded_shape = {h1 , w1 , 3};
        float[] window = window = get_window(images , min_dim , min_scale , max_dim ,mode , bitmap);
        float scale = scale(images , min_dim , min_scale , max_dim , mode , bitmap);


        image_meta = compose_image_meta(0 , image_shape , molded_shape , window , scale , zeros);

        float[][] image_metas = {image_meta};


        return image_metas;
    }

    private float [][][] mold_image(float[][][] images,  Object o){
        float[] mean_pixel = {(float) 123.7, (float) 116.8, (float) 103.9};
        float mini = 0.0f;
        float maxi = 0.0f;
        for(float[][] image : images){
            for(float[] img : image){
                for(int i = 0 ; i < img.length ; i++){
                    float temp = img[i] - mean_pixel[i];
                    img[i] = temp;
                }
            }
        }
        return images;
    }

    public int[][] unmold_detection(float[][][] output , int[] window){
        float detections[] = new float[100];
        int count = 0;
        float[][] boxes = new float[100][4];
        ArrayList<Integer>zero_idx = new ArrayList<>();
        int n = 0 , N;
        for(int i = 0 ; i < 1 ; i++){
            for(int j = 0 ; j < 100 ; j++){
                detections[n++] = output[i][j][4];

            }
        }
        for(int i = 0 ; i < 1 ; i++){
            for(int j = 0 ; j < 100 ; j++){
                if(output[i][j][4] == 0){
                    zero_idx.add(j);
                }
            }
        }
        if(zero_idx.size() > 0){
            N = zero_idx.get(0);
        }
        else {
            N = detections.length;
        }

        for(int i = 0 ; i < 1 ; i++){
            for(int j = 0 ; j < 100 ; j++){
                for(int k = 0 ; k < 4 ; k++){
                    boxes[j][k] = output[i][j][k];
                }
            }
        }

        int[] image_shape = {1024 , 1024};

        float[] windows = norm_boxes(window , image_shape);
        float wy1 = windows[0];
        float wx1 = windows[1];
        float wy2 = windows[2];
        float wx2 = windows[3];
        float[] shift = {wy1 , wx1 , wy1 , wx1};

        float wh = wy2 - wy1;
        float ww = wx2 - wx1;
        float[] scale = {wh , ww , wh , ww};

        for(int i = 0 ; i < 100 ; i++){
            for(int j = 0 ; j < 4 ; j++) {
                float temp = boxes[i][j] - shift[j];

                boxes[i][j] = temp / scale[j];

            }
        }
        int [] shape = {1024 , 1024};
        int[][] new_box = denorm_boxes(boxes , shape);


        return new_box;
    }
    private float[] norm_boxes(int[] window , int[] image_Shape){
        int h = image_Shape[0];
        int w = image_Shape[1];
        int [] scale = {h - 1 , w - 1 , h - 1 , w - 1};
        int[] shift = {0,0,1,1};
        int [] test = new int[4];
        float[] norm_box = new float[4];
        for(int i = 0 ; i < shift.length ; i++){
            test[i] = (window[i] - shift[i]);
        }
        for(int i = 0 ; i < test.length ; i++){
            norm_box[i] = test[i] / scale[i];
        }

    return norm_box;
    }
    private int[][] denorm_boxes(float[][] boxes , int[] shape){
        int h = shape[0];
        int w = shape[1];
        int [] scale = {h - 1 , w - 1 , h - 1 , w - 1};
        int[] shift = {0 ,0 , 1 , 1};
        int[][] denorm_box = new int[100][4];
        int[][] new_box = new int[100][4];
        for(int i = 0 ; i < 100 ; i++){
            for(int j = 0 ; j < 4 ; j ++){
                float temp = boxes[i][j] * scale[j];
                new_box[i][j] = (int)Math.round(temp + shift[j]);

            }
        }


        return new_box;
    }



}

