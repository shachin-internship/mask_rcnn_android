package com.example.myapplication;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageDecoder;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.checkerframework.checker.units.qual.C;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.support.image.ImageProcessor;

import android.graphics.Matrix;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import android.graphics.Canvas;




public class MainActivity extends Activity {
    private final static String TAG = "MainActivity";
    private ImageView imageView;
    private TextView textview;
    private ImageView imageViewbyCode;
    private LinearLayout myLayout;
    private AssetManager assetManager;
    private int inputSize = 1024;
    private Integer config = null;
    Interpreter interpreter;
    private  int REQUEST_OPEN_IMAGE = 1000;
    Uri imageUri;
    private objectDetectorClass objectDetectorClass;
    private int[] intValues = new int[inputSize * inputSize];
    Bitmap bitmap , bitmap2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (ImageView)findViewById(R.id.image);
        myLayout = (LinearLayout)findViewById(R.id.myLayout);
        textview = (TextView)findViewById(R.id.textView);

        assetManager = getAssets();

        try{

            objectDetectorClass = new objectDetectorClass(getAssets() , "detect.tflite" , "labels.txt" );
            Log.d("MainActivity" , "Model is loaded successfully");
        }
        catch(IOException e){
            Log.d("MainActivity" , "getting some error");
            e.printStackTrace();
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE} , 0);
            }
        }

    }

    public void openimage(View v){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");

        startActivityForResult(intent , REQUEST_OPEN_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            if(requestCode == REQUEST_OPEN_IMAGE){
                Uri uri = data.getData();
                bitmap2 = loadfromuri(uri);
                imageView.setImageBitmap(bitmap2);
            }
        }
    }
    public Bitmap loadfromuri(Uri uri){
        Bitmap bitmap2 = null;

        try {
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.O_MR1){
                ImageDecoder.Source source = ImageDecoder.createSource(getContentResolver() , uri);
                bitmap2 = ImageDecoder.decodeBitmap(source);
            }
            else {
                bitmap2 = MediaStore.Images.Media.getBitmap(getContentResolver() , uri);
            }
        }catch (IOException e){
            e.printStackTrace();
        }

        return bitmap2;
    }

    public void displayOneImage(View v) throws IOException {
        if (imageViewbyCode != null) {
            imageViewbyCode.setVisibility(View.GONE);
        }
        imageView.setVisibility(View.VISIBLE);
        bitmap = null;

        bitmap = bitmap2.copy(Bitmap.Config.ARGB_8888 , true);

        long start2 = System.currentTimeMillis();
        float [][][][] molded_image = objectDetectorClass.image_mold(bitmap);
        float[][] image_meta = objectDetectorClass.image_metas(bitmap);
        float[][][] anchor = new float[1][261888][4];


        String text = "";
        try{
            InputStream is = getAssets().open("anch_sam.txt");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            text = new String(buffer);

        }catch (IOException ex){
            ex.printStackTrace();
        }

        int k = 0;
        float[] sam = new float[261888 * 4];
        String temp = "";
        for(int i = 0 ; i < text.length() ; i++){
            if(text.charAt(i) != ' '){
                temp += text.charAt(i);
            }
            else{

                sam[k++] = Float.parseFloat(temp);
                temp = "";
            }
        }
        int m = 0;
        for(int i = 0 ; i < 1 ; i++){
            for(int j = 0 ; j < 261888 ; j++){
                for(int l = 0 ; l < 4 ; l++){
                    if(m < sam.length){
                        anchor[i][j][l] = sam[m++];
                    }

                }
            }
        }
        long end2 = System.currentTimeMillis();
        System.out.println("Getting molded_image , image_meta , anchor : " +((end2 - start2)) + " ms");


        long start = System.currentTimeMillis();
        Interpreter inter = objectDetectorClass.interpreter;
        Object[] inputs = {molded_image , image_meta , anchor};

        float[][][] output = new float[1][100][6];
        Map<Integer , Object> outputs = new HashMap<>();
        outputs.put(0,output);
        inter.runForMultipleInputsOutputs(inputs , outputs);
        long end = System.currentTimeMillis();
        System.out.println("Loading the interpreter: " +((end - start)) + " ms");

        int[] window = {0,0,1024 , 1024};
        int [][]boxes = objectDetectorClass.unmold_detection(output , window);

        ArrayList<Integer> class_id = new ArrayList<>();

        for(int i = 0 ; i < 1 ; i++){
            for(int j = 0 ; j < 100 ; j++){
                if(class_id.contains((int)output[i][j][4])){
                    continue;
                }
                else {
                    class_id.add((int)output[i][j][4]);
                }
            }
        }
        ArrayList<String> name = new ArrayList<>();
        String[] class_name = {"" , "Baton" , "Pliers" , "Hammer" , "Powerbank" , "Scissors" , "Wrench" , "Gun" , "Bullet" , "Sprayer" , "HandCuffs" , "Knife" , "Lighter"};
        String s = "";
        for(int i : class_id){
            s += class_name[i];
            s += " ";
        }


        textview.setText(s);
        Bitmap bitmap1 = Bitmap.createScaledBitmap(bitmap , 1024 , 1024 , false);
        Bitmap mutableBitmap = bitmap1.copy(Bitmap.Config.ARGB_8888, true);
        long start1 = System.currentTimeMillis();

        Canvas canvas = new Canvas(mutableBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(8f);
        for(int i = 0 ; i < 100 ; i++){
            float y1 = boxes[i][0];
            float x1 = boxes[i][1];
            float y2 = boxes[i][2];
            float x2 = boxes[i][3];
            canvas.drawRect(x1 , y1 , x2 , y2, paint);
        }

        imageView.setImageBitmap(mutableBitmap);
        long end1 = System.currentTimeMillis();
        System.out.println("Post Processing : " +((end1 - start1)) + " ms");
    }




}